<?php

/**
 * @file uc_recurring_cc_notify.ca.inc
 * This file contains the ubercart Conditional Action hooks and functions
 * for the UC Recurring Credit Card Notify module.
 */

/******************************************************************************
 * Conditional Action Hooks                                                   *
 ******************************************************************************/

/**
 * Implementation of hook_ca_trigger().
 */
function uc_recurring_cc_notify_ca_trigger() {
  $triggers = array();

  $order = array(
    '#entity' => 'uc_order',
    '#title' => t('Order'),
  );
  $recurring_fee = array(
    '#entity' => 'uc_recurring_fee',
    '#title' => t('Recurring Fee'),
  );

  $triggers['uc_recurring_cc_notify_upcoming_expiry'] = array(
    '#title' => t('Upcoming recurring fee credit card expiry'),
    '#category' => t('Recurring fees credit card notifications'),
    '#arguments' => array(
      'order' => $order,
      'recurring_fee' => $recurring_fee,
    ),
  );

  $triggers['uc_recurring_cc_notify_upcoming_charge'] = array(
    '#title' => t('Upcoming recurring fee charge'),
    '#category' => t('Recurring fees credit card notifications'),
    '#arguments' => array(
      'order' => $order,
      'recurring_fee' => $recurring_fee,
    ),
  );

  return $triggers;
}
