======================================================
Ubercart Recurring Payments Credit Card Notifications
======================================================

This module provides additional functionality to ubercart recurring payments
that use credit cards.
It adds the following ubercart conditional actions triggers
(http://www.ubercart.org/docs/user/7657/configuring_conditional_actions):

* Upcoming recurring fee credit card expiry
* Recurring fees credit card notifications

You can also set the lead time for these notifications, so you can control
how far in advance of these events the triggers are pulled.

This module currently does not supply any conditional actions predicates
so you have to create your own, using the provided triggers.

======================================================
Requirements
======================================================

You need the following modules:
Ubercart 6.x-2.x - http://drupal.org/project/ubercart
UC Recurring 6.x-2.x - http://drupal.org/project/uc_recurring

Currently you also need to patch UC Recurring with the patch in the issue
at http://drupal.org/node/1195892

======================================================
Installation
======================================================

1. Make sure the required modules are installed.
2. Patch the UC Recurring module with the patch in the issue at
http://drupal.org/node/1195892
For further information on patches see http://drupal.org/patch
3. Install this module as per any other drupal module.
See see http://drupal.org/node/70151 for further information.

======================================================
Configuration
======================================================

- The module settings can be found at
www.yoursite.com/admin/store/settings/payment/edit/recurring-cc-notify

Here you can set which triggers are enabled (it is good to disable them if they
are not being used to save on unnecessary processing time during cron) as well
as the lead time of the triggers.

- The ubercart conditional actions administration page is found at
www.yoursite.com/admin/store/ca

Here you can set up your conditional actions, for example you might want a
predicate that uses the "Upcoming recurring fee credit card expiry" trigger
and the "Send an order email" action to send the customer out an email
notifying them that their credit card will expire soon so they need to
update their details at the website.

======================================================
Credits
======================================================

This module was written by and is maintained by Agileware
(drupal.org/user/89106 - agileware.net)

This module is adapted from original code taken from Evan Donovan's
(drupal.org/user/168664) Credit Card Expiration Notify sandbox project
(drupal.org/sandbox/evandonovan/1106348).

