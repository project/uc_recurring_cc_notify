<?php

/**
 * @file
 * Administration page callbacks for the UC Recurring Credit Card Notify module.
 */

/**
 * Recurring payment settings form.
 */
function uc_recurring_cc_notify_settings_form() {
  $form = array();

  $form['expiry'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upcoming recurring fee credit card expiry'),
    '#description' => t("Settings for the ubercart conditional actions 'Upcoming recurring fee credit card expiry' trigger."),
  );
  $form['expiry']['uc_recurring_cc_notify_expiry_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable credit card expiry notification'),
    '#description' => t('Enable/disable the trigger.') . '<br />' .
                      t('If you are not using this trigger you should disable this to save processing time during cron.'),
    '#default_value' => variable_get('uc_recurring_cc_notify_expiry_enabled', TRUE),
  );
  $form['expiry']['uc_recurring_cc_notify_expiry_days'] = array(
    '#type' => 'select',
    '#title' => t('Expiry notification days'),
    '#description' => t('The number of days before the credit card expires to pull the trigger.') . '<br />' .
                      t('Note that the expiry is considered to be the first day of the expiry month.'),
    '#options' => array_combine(range(1, 60), range(1, 60)),
    '#default_value' => variable_get('uc_recurring_cc_notify_expiry_days', 7),
  );
  $form['charge'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upcoming recurring fee charge'),
    '#description' => t("Settings for the ubercart conditional actions 'Upcoming recurring fee charge' trigger."),
  );
  $form['charge']['uc_recurring_cc_notify_charge_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable fee charge notification'),
    '#description' => t('Enable/disable the trigger.') . '<br />' .
                      t('If you are not using this trigger you should disable this to save processing time during cron.'),
    '#default_value' => variable_get('uc_recurring_cc_notify_charge_enabled', TRUE),
  );
  $form['charge']['uc_recurring_cc_notify_charge_days'] = array(
    '#type' => 'select',
    '#title' => t('Recurring fee charge notification days'),
    '#description' => t('The number of days before the recurring fee charge to pull the trigger.') . '<br />' .
                      t('Note that the expiry is considered to be the first day of the expiry month.'),
    '#options' => array_combine(range(1, 60), range(1, 60)),
    '#default_value' => variable_get('uc_recurring_cc_notify_charge_days', 14),
  );

  return system_settings_form($form);
}
